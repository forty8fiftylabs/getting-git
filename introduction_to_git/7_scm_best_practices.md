# SCM Best Practices  
---

- Commit often! It’s like auto-save! Get in a habit of making commits often during your day.  

- Make sure you have the latest version of your code. Once your team starts using SCM you’ll find rapid changes in the project all the time. Make sure to have the latest versions as you work.  

- Make detailed notes. Commit messages need to be concise yet detailed so you can look at a glance what changes were made to the code.

- Review changes before committing.  

- Use branches. Especially if you have repos being used in production. It minimizes risk.  

- Do not commit secrets! Use a `.gitignore` file for excluding certain files from being tracked in git. Go [here](https://github.com/github/gitignore) to find some gitignore templates.

---
**Navigation**

* [Previous Slide](5_basic_git_commands.md)
* [Table of Contents](README.md)