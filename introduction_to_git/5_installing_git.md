# Installing Git  
---

- We're not covering the installation of Git. This varies based on your system, so go here to install Git -- https://www.atlassian.com/git/tutorials/install-git.

- Once installed you'll have access to Git via the CLI but you can also leverage GUI-tools like Sourcetree to manage your repositories and run Git commands.

- You can install Sourcetree from here -- https://www.sourcetreeapp.com/.

- The resources slide has more GUI options. 

---
**Navigation**

* [Previous Slide](4_basic_terminology.md)
* [Next Slide](6_basic_git_commands.md)
* [Table of Contents](README.md)