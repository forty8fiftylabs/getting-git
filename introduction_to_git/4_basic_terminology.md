# A Basic Git Glossary  
---

- _DVCS_ - Distributed Version Control System

- _fork_ - A copy of a repository owned by someone else

- _git_ - An open source DVCS developed by Linus Torvalds

- `origin` - is a shorthand name for the remote repository that a project was originally cloned from.  

- _SCM_ - Source Code Management  

- _Repository_ - Where all your code, script, images, files, live. Can include folders and binary objects.  

- _Version Control_ -  A way to track multiple versions of the same file. A history of all those changes.

---
**Navigation**

* [Previous Slide](3_how_git_works.md)
* [Next Slide](5_installing_git.md)
* [Table of Contents](README.md)