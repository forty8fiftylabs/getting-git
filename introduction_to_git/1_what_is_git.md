# What is Git?
---

- Git is an open source version control system originally developed by the founder of Linux - Linus Torvalds - in 2005. Originally developed to help coordinate the development of the Linux kernel it has grown exponentially since then!  

- 100% free and open-source and under the GNU GPLv2 license  

- It is one of the most popular source control management (SCM) systems available today.  

- One of it's biggest strengths is that it is a **distributed version control system (DVCS)** and adaptable to a variety of workflows

- It has spawned numerous business/products that support enterprise Git:  
    - Atlassian Bitbucket
    - GitHub
    - GitLab
    - And many more

----
**Navigation**

* [Next Slide](2_why_use_scm.md)
* [Table of Contents](README.md)