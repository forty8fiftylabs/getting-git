# Basic Git Commands to Know  
---

- `git add` - Add file(s) to the repository index for tracking  

- `git branch` - Used to list, create, and even delete branches within git. 

- `git checkout` - Used to switch between different branches.  

- `git clone` - Clones an existing repository into a new directory. If the directory does not exist it will create that directory.  

- `git commit` - Makes a commit to git and when used with the `-m` flag you can insert the message on the line (i.e. `git commit -m 'My first commit'`)  

- `git config` - Get and set your options  

- `git fetch` - Downloads history from remote tracking branches

- `git init` - Create an empty git repository and initialize it or turn an empty directory into a git repository.   

- `git merge` - Takes independent code branches and merges them together.  

- `git pull` - Used to fetch and download content from a remote repository and update your local repository to match the remote. This command fetches and merges upstream changes to your local repo.  

- `git push` - Push your local changes upstream after a commit

- `git reset` - Totally mess something up? Use git reset to undo commits

- `git status` - Shows the current working tree status (any modified files, etc)

More can be found here -

- [Atlassian Git Cheat Sheet](https://www.atlassian.com/git/tutorials/atlassian-git-cheatsheet)

- [GitHub Git Cheat Sheet](https://github.github.com/training-kit/downloads/github-git-cheat-sheet.pdf)

---
**Navigation**

* [Previous Slide](4_basic_terminology.md)
* [Next Slide](6_scm_best_practices.md)
* [Table of Contents](README.md)