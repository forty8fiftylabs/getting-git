# Why use SCM or Version Control?
---

- Provides a way to securely store your "code" that isn't (potentially) on your local environment.

- Using Git opens up integrations with other DevOps-related tooling and processes.  

- Provides a detailed history of all code commits so that at any point in time you can go back and audit the history of the code. You can even roll back and revert changes that might have been made in mistake!  

- Allows you to go on vacation without being paged to email someone some obscure script!  

- It protects your work and you.  

- **GitOps** is gaining popularity. What is GitOps? It is leveraging Git as the SCM behind code-based infrastructure and operational procedures. Git becomes the source-of-truth and all things flow from Git to the systems downstream.  More info on GitOps can be found [here](https://www.atlassian.com/git/tutorials/gitops).

---
**Navigation**

* [Previous Slide](1_what_is_git.md)
* [Next Slide](3_how_git_works.md)
* [Table of Contents](README.md)