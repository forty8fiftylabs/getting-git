# Table of Contents
---

1. [What is Git?](1_what_is_git.md)  
2. [Why us SCM?](2_why_use_scm.md)
3. [How Git works](3_how_git_works.md)
4. [Basic Terminology](4_basic_terminology.md)  
5. [Basic Git Commands](5_basic_git_commands.md)
6. [SCM Best Practices](6_scm_best_practices.md)

----
**Navigation**

* [Next Slide](1_what_is_git.md)
