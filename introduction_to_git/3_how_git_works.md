# An Overview on How Git Works
---

- You create or initialize a repository (locally or online - Bitbucket, GitHub, Gitlab, etc).  

- You clone that repository to your local machine (unless you've created it locally).  

- You add your scripts, code, text files, etc. Anything an Operations/SysAdmin/Network Admin/Security/DevOps person uses!  

- You then make a commit to save those changes in Git and start tracking those files.   

- You then push those changes up to the `master` branch (or another branch if you have them).

- Another person can then pull those changes to their local machine to work on the project as well.  

-  Other people can contribute to the project by submitting their code changes using a pull request or merging their code into the project.   

---
**Navigation**

* [Previous Slide](2_why_use_scm.md)
* [Next Slide](4_basic_terminology.md)
* [Table of Contents](README.md)