# Table of Contents
---

1. [Create a Bitbucket Account](1_intro_create_an_account.md)  
2. [Create Your First Repo ](2_create_your_first_repo.md)
3. [Clone Your Repo](3_clone_your_repo.md)
4. [Making Your First commit](4_making_your_first_commit.md)  
5. [Pushing Your Code Upstream](5_pushing_your_code_upstream.md)
6. [Branching](6_branching.md)
7. [Merging Your Branches](7_merging_branches.md)
8. [Pulling Changes from Bitbucket](8_pulling_changes_downstream.md)
9. [Using Sourcetree or Another Git UI Tool](9_using_sourcetree.md)

----
**Navigation**

* [Next ~ Create a Bitbucket Account](1_intro_create_an_account.md)

