# Step 5: Pushing your Code Upstream
---

Ok, now you've successfully created a repo, added some files, and made your first commit! Now what?

Time to push this code from your local workstation to Bitbucket/GitHub/Gitlab, etc.

Why do you need to push your code upstream? Well, for starters it gets a copy off your local system and into a remote SCM tool to be managed. That way if your local system is offline, or you lose data, then you will still be able to pull the latest version from the upstream SCM source.

Secondly, and quite important for teams, is that by pushing the code upstream you are  making that code available for others to reuse, check out, and develop against. In other words: collaboration is easier! No more emailing files back and forth. New staff can just pull the latest versions of your SCM and use them in their projects.

Let's get started!

## Pushing Code to Bitbucket  
---

Lots of If's incoming...

If you try to push your code to Bitbucket you might get an error message if your remote URL is not set yet. If you just cloned the repo from Bitbucket then it should set the remote URL automatically. You can verify this by typing in `git remote -v` and that will show you the remote URL's for fetching and pushing. Feel free to skip the steps that have you adding the remote URL if that is the case.

If you created the repo from the CLI, then you will have to set your remote URL manually. Creating a git repo from the CLI does not automatically add the remote URL off the top.  Follow the steps below to add your remote url in that case:

### Note: Discovered that something might break using the CLI aspect. So skip this for now or just beware that it might break your existing origin remote url's. 

- Change to your repo in your CLI

- Connect the repo to Bitbucket with the following commands:  
    - The syntax is `git remote add <name> <url>`  
    - For my example it would be: `git remote add origin https://Allen-BBDemo@bitbucket.org/Allen-BBDemo/my-first-repo.git`  
    - Your URL will be look similar. Just with your username and repo name.  
    - Again, you have 2 options `HTTPS` or `SSH`. For today we are using the `HTTPS` option.  
    - The `origin` is reference to the URL from within Git.  

Whether you added the remote URL manually or it was added automatically you will now need to push your changes upstream.  

- Push the changes up to your SCM provider (in this case Bitbucket) with the following command:  `git push -u origin master`  
    - This tells git to push your code up to your `origin` url and put it on the `master` branch.  

- You may or may not be prompted for a password initially. So enter your Bitbucket password if prompted.  

  After a few seconds you should see something similar to the following:  

  ![Git push](images/6_pushing_your_code_upstream.png)

Congrats! You have pushed your code successfully upstream to Bitbucket!  

You can log into Bitbucket and see the commit!  

  ![Git push](images/6_pushing_your_code_upstream_2.png)

### Now let's dig into Branching!  

----
**Navigation**

* [Previous ~ Making Your First Commit](4_making_your_first_commit.md)
* [Next ~ Branching](6_branching.md)
* [Table of Contents](README.md)