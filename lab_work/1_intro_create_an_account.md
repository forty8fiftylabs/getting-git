# Step 1: Create an Account
---

- Time to put knowledge to practice:
    - If you have access to your corporate SCM tool that is Git-based you might want to request to gain access to that tool. 
    - For this workshop, we will be creating an account online using [Bitbucket.org](https://www.bitbucket.org). You can also use [GitHub.com](https://www.github.com/), [GitLab.com](https://www.gitlab.com/) or other Git hosting sites.
    - Creating an individual account is free on most of these services. No need to enter any billing/credit card information to get started!

- Navigate to www.bitbucket.org and click on the "Get started for free" button  

    ![Create an Account](images/2_create_an_account.png)  

- Follow the prompts/new user creation wizard to create you free account. If you already have an account with Bitbucket you can just login instead.  

- Once you’ve confirmed your account and have logged in you will be presented with a dashboard and some options. We’re going to concentrate on the _Create Repository_ option for now.  

----
**Navigation**

* [Next ~ Create Your First Repo](2_create_your_first_repo.md)  
* [Previous ~ Table of Contents](README.md)
* [Table of Contents](README.md)