
# Step 3: Clone Your Newly Created Repo!

![Clone Repo](images/clone_yoda.jpg)  

Now that you have your repo created you’ll see a number of options on your dashboard. Bitbucket and Git can become complicated quite fast but for our purposes we are not going to worry about most of these options. You can take time to click and read up on them, but don't stress if you're overwhelmed at the dashboard.  

What we want to do is connect this repo with your local development system so you can push you code to Bitbucket!  

* Copy the `git clone` link that you see presented on your screen. If you cannot find it there, you can find it by clicking the **Clone** button and copying that link. See the image below for where to find the link:  

    ![Create your 1st Repo](images/4_clone_your_new_repo.png)  

* You will have 2 options in copying the repo link: **HTTPS** and **SSH**. We want to use the HTTPS link. If you do not see it of the top then look for a drop-down menu and select it from there.  
* Your link should look similar to this one:   
    * HTTPS authentication:   
    
        `git clone https://Allen-BBDemo@bitbucket.org/Allen-BBDemo/my-first-repo.git`  
        
    * SSH authentication:  
    
        `git clone git@bitbucket.org:Allen-BBDemo/my-first-repo.git`  


        >A note on authentication. If you use HTTPS, you will be prompted for your password. If you use SSH, then you will need to create an SSH key and upload it to your SCM provider in order to pull, push, and work with repos.

* Open your terminal/CLI tool and navigate to where you want to store your repository. When you clone the repo it will automatically create the repo directory and copy all the files into it.

* If you have an existing folder that has code that you want to push up to Bitbucket then we’ll cover that process later.

* When you are at the folder level to where you want to clone the repo then paste the HTTPS (or SSH) command above and hit enter. Depending on the size of the repo it will take a few seconds to create the folder structure and copy all the files down to your local system.  

    You should see something similar to the following image:

    ![Cloning your repo](images/5_cloning_your_repo_cli.png)

    >Note: If you created a repo using the CLI and not using Bitbucket there is nothing to clone, so you can bypass this step.

### Ok! We have our repo created, cloned, and now let's go make our first commit!
----
**Navigation**

* [Previous ~ Create Your First Repo](2_create_your_first_repo)
* [Next ~ Making Your First Commit](4_making_your_first_commit.md)
* [Table of Contents](README.md)