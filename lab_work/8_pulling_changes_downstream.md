# Step 8: Pulling changes from Bitbucket  
---

Say you've been on a much-needed vacation and when you get back into the office you need to get the latest updates to your awesome Ansible playbooks or Terraform files in order to add some changes.  

You could immediately jump right in and start developing and push your changes upstream but while you were out the other SysAdmin/Operations folks were making their own changes and the code on bitbucket is a few commits ahead of yours.  

What do you do?  

Well, we defintely do not want to just blind push our stuff upstream as that will cause some potential merge conflicts so let's pull that code down first before we begin developing.  

Let's get started!  

## Add files to Bitbucket  

To simulate having another person working on the project we are going to just create some files manually within Bitbucket and then pull them to our local repo.  

* Navigate to your Bitbucket page and go to your repo. When there click on the `Add File` option in the dropdown on the top-right hand side of your repository.  

  ![Git pull in action](images/7_pulling_from_bitbucket.png)  

* It will open up a new screen and you can actually add new files directly into Bitbucket! You should see a screen like this:  

  ![Git pull in action](images/7_pulling_from_bitbucket_2.png)  

* Go ahead and type in anything or paste a script, etc. Then hit **Commit**. You'll need to add a commit message and once done it will commit this new file just like you would do it from the CLI.  

* After adding and committing the files you'll see a summary screen showing the commit, an approve button, and a bunch of other data. You can ignore those for now and just click back on the `Source` link on the left to get back to the main repo page. From that page you can see all the new file(s) that were manually added.

* If you want to create more files feel free to do so. Once done let's pull them downstream!  

## Pulling downstream  

Once again, open your terminal and navigate to your repository directory. We want to see what new changes have been  made in the project while we were out.  

  * `git fetch` - will let update all the repo with all the commits, history, etc but will not merge it into your project  
  
  * `git pull` - does the same thing but updates your local repository to be in sync with the remote repository  

If you want to immediately update your local repo and merge it automatically then use the `git pull` command. It's a little more aggressive but if you are a solo operations person or maybe working with a small team and you know there are no conflicts then this makes things potentially easier as you'll have to run less commands to get your project in sync.  

If you want to review the changes to make sure they will not conflict with your local repo then you can run the `git fetch` command. After reviewing you can then do a local merge to bring your repo in synch with the remote repo.  

For our example we are going to run the `git pull` to update as we do not have any files different locally than what is on the remote because before we left for vacation we did a commit and pushed all our changes upstream.

  ![Git pull](images/8_pulling_upstream_changes.png)  

You should see a similar output to the image above showing that there were some new files that were pulled and merged into our local repo.  

Now if you look in the folder with a `ls` command you can see the new files that were added. Your local repo is now in sync with upstream and you can start adding new changes and files to the repo.  

### Congratulations! You've successfully (hopefully) created a repo, made changes, pushed and pulled code! You're on your way to leveraging Git in your day-to-day flow!

----
**Navigation**

* [Previous ~ Merging](7_merging_branches.md)
* [Next ~ Using Sourcetree or Another Git UI Tool](9_using_sourcetree.md)
* [Table of Contents](README.md)