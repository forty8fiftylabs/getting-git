# Step 7: Merging Your Branches
---

Now that we have our branch created, files committed, and pushed to Bibucket, we need to merge that code into the `master` branch.

Merging can be a little overwhelming initially, but with a few simple commands you'll be able to merge your branches safely!

## Merge Branch Steps

* Navigate to your repo directory in your CLI if you are not there already, and type this in the terminal:  `git status`.  Most likely you are still on the `development` branch. So we are going to switch to our `master` branch in order to receive the merged code. Key point is that you want to be in the branch that will receive the merge, in our case it is the `master` branch.  

* Switch to the `master` branch:  `git checkout master`  

* Make sure you're `master` is up-to-date with the latest commits using the `git fetch` command.  

* Then merge the branches:  `git merge development`  

* You should see something similar to this:  

  ![Git merge](images/7_merging_branches.png)

  The `git merge` command merges your `development` branch into the `master` branch.  

* Push the changes upstream: `git push origin` 
  
  Visit your repo on bitbucket.org and you should see the changes merged into your `master` branch now.  

* One of the tenets of DevOps thinking is - short-lived feature branches. So you can delete the development branch with the following command:  `git branch -d development`  

    This helps clean up your repo so you do not have a bunch of branches sitting out there and active.  This is not a required step but many times it's part of a DevOps process to have short-lived branches.   
    
    Of course how you build out your branch strategy is up to you and your organization, so you need to architect and determine how you want to handle branches.  

### Let's pull other changes downstream now!
  
----
**Navigation**

* [Previous ~ Branching](6_branching.md)
* [Next ~ Pulling Changes from Bitbucket](8_pulling_changes_downstream.md)
* [Table of Contents](README.md)