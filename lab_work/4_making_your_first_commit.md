# Step 4: Making your 1st Commit!
---

Now onto the fun stuff!

You should have an empty repository that you have cloned form BitBucket (although it might have a template `README.md` file if you had added that option). So we are going to start adding some files now and making our first commit!

Git doesn't really care what you store in it - be it text files, PowerShell scripts, Ansible playbooks, Terraform manifests, Java code, or even images! Git can store, track, and version all of them!

Files can be added manually by dragging, copying, or even creating them in the directory itself. For this tutorial we are just going to create some plain text files using the CLI.

* From your terminal go ahead and navigate to your repo directory as done in Step 3. If you're already there then skip this step.

* Now let's create a simple text file by echoing some content from the CLI. From your CLI type in the following:  `echo "My first file!" >> my_first_file.txt`  

  Feel free to change it up if you want to. Add some existing scripts, or whatever you want.

* Now in your CLI type in `git status` and you should see a message similar to the following:  

  ![Create your 1st Repo](images/5_making_your_first_commit.png)  

  What this is telling us, is that Git sees the new file but it is not tracked in SCM yet. So we now need to add this file to Git for tracking!  

* Type in `git add my_first_file.txt` to add this file to Git.   

  You can also use the command `git add .` to add all untracked files if you have more than one file to add.  

* Now if you type in `git status` again you should see a slightly different status:  

  ![Git status](images/5_making_your_first_commit_2.png)

Git now sees the file and will be tracking it from now on! All we need to do is finalize it with a commit.

* Type in `git commit -m "My 1st commit"`  

  You should see something similar to the following:

  ![Git commit](images/5_making_your_first_commit_3.png)

You've made your first commit! Aw yeah! With the `git commit` command you've told git to save this info, and put in a small descriptive message.

Once again if you type in `git status`, then you'll see a message saying that `Your branch is ahead of the orign\master`. Git is letting you know that your local commit is not synched up with the remote branch on Bitbucket yet. Let's solve that next!

### What next? Let's push these changes from our local system to Bitbucket so that others can help collaborate on this project!

----
**Navigation**

* [Previous ~ Clone Your Repo](3_clone_your_repo.md)
* [Next ~ Pushing Your Code Upstream](5_pushing_your_code_upstream.md)
* [Table of Contents](README.md)