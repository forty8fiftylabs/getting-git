
# Step 2a: Create a Repo Using Bitbucket #
---

When you click the _Create Repository_ button on your dashboard you will see a screen similar to the one below.  

> Note: 
>
>If you have an existing repository that you want to import, then click that button instead and follow the instructions. For our purposes we’re going to ignore that for now and create a brand new repository.

![Create your 1st Repo](images/3_create_your_first_repo.png)

Let’s break this process down:  

- **Workspace** - should be your own name unless you have a team you’re part of already. In that case you can pick a different workspace if one is available or create a new workspace.  

- **Project name** - the name of your project. Maybe it’s Operations, or DevOps, or Testing Git, make it easy to understand.  

- **Repository name** - The name of your code repository. Make sure to make it sensible if you are using production code. That way you can identify it at a glance.  

- Leave **Access level** private unless you are wanting to make your code publicly available. For the most part you want to leave it private. If you are wanting to make your source code Open Source then you can make it private.  

> Note: 
>
> You will most likely want to incorprate a license if you open source your code. Check with your legal team as this is not something we will cover in this workshop.

- **Include a README** if you want. Always recommend to incldue a basic README document. This helps users understand what this repository is about. You can use normal Markdown syntax for editing the document.  
    
    For more information on Bitbucket README formatting go [here](https://confluence.atlassian.com/bitbucket/readme-content-221449772.html).

- **Advance settings**. Leave that alone for now.  

- Once done then hit that **Create repository** button!  

# Step 2b: Using the CLI to Create a Repo
---

If you want to create a repo from the CLI instead of using the bitbucket web-based interface then follow the steps below. 

These steps are based on MacOS/Linux commands. If you are a Windows user you can use WSL2 or the equivalent Windows/Powershell commands.

* Open up your favorite terminal application and navigate to where your code will be (if not created yet), or go into your existing folder for any projects you already have in progress.
* Then do the steps below:
    * Create a directory if needed: `mkdir <your_folder_name>`

    * Or navigate to an existing or newly created directory: `cd <your_folder_name>`

    * Type in: `git init` And it will initialize that folder for Git. 
     
* If you do a `ls -alt` you'll see a hidden folder called `.git`. Don't remove this as it stores all the Git information!
    
There you go! You've created your 1st repository in Git! Now it's time to add some files!

----
**Navigation**

* [Previous ~ Create a Bitbucket Account](1_intro_create_an_account.md)
* [Next ~ Clone Your Repo](3_clone_your_repo.md)
* [Table of Contents](README.md)