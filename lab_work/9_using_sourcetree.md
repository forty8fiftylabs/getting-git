# Step 9: Using Sourcetree or another Git UI tool  
---

Learning Git fundamentals using the CLI is important, but as you gain more experience you will most likely want to incorporate a GUI-type of tool to help manage repositories.  

The list below is just a smattering of available options out on the web. Many of these products are free. If you have a development team at your company it might be helpful to see what they are using or have standardized on and maybe even if they have extra licenses for you to utilize.  

Here are a few of the top UI tools available:  

* _Sourcetree_ by Atlassian -- https://www.sourcetreeapp.com/  
* _GitHub Desktop_ by GitHub -- https://desktop.github.com/  
* _GitKraken_ -- https://www.gitkraken.com/  
* Other GUI options -- https://git-scm.com/downloads/guis  

Here's a snapshot of what Sourcetree looks like with our workshop repo:  

![Sourcetree view](images/9_sourcetree_1.png)  

And you can manage repos from multiple Git providers as well!  

![Sourcetree view](images/9_sourcetree_2.png)  

# IDE's and Text Editors

- _Visual Studio Code_ -- https://code.visualstudio.com/
- _Sublime Text_ -- https://www.sublimetext.com/  
- _Notepad++_ -- https://notepad-plus-plus.org/


# Conclusion  

Git and SCM can be confusing and overwhelming at first, but with a little bit of practice, you can learn it and start becoming more efficient at your work. It is a critical skill for modern SysAdmins, DevOps, SRE's, Network Operations, Security teams and more!  

Even if you just pull the absolute basics of using `git init`, `git add`, `git commit` in order to have your work in a version-controlled environment then that's a win. 

# Links for further learning  

I'm grateful that I didn't have to re-invent the wheel for this course! The content from here came from a variety of web-based resources that you can access as well. I've listed them here for your reference:  

* _Atlassian Getting Git Right_ -- https://www.atlassian.com/git  
* The Git SCM website -- https://git-scm.com/doc  
* Git cheatsheet -- https://github.github.com/training-kit/downloads/github-git-cheat-sheet/  
* GitHub Labs -- https://lab.github.com/  

----
**Navigation**

* [Previous ~ Puling Changes Donwstream](8_pulling_changes_downstream.md)  
* [Table of Contents](README.md)