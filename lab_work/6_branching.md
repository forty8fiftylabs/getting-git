# Step 6: Branching
---

One of the amazing things about Git is that it enables you to experiment and work in branches that do not affect the core code.

Use case: You have a working Ansible playbook that does patches some of your systems but you need to make changes to it but at the same time do not want to break what is already working while you update the playbook. In comes Branching!

The idea behind branches in Git is to be able to work on something in a safe, isolated manner. Then when you are ready for production you can merge that branch back into the core project (master).

Let's go back to our current project and see it in action!

## Branching Basics

* Open up your CLI and type this in the terminal:  `git checkout -b  development`  
  
    What will happen is that Git will create a new branch called `development` and checkout that branch.  

* You can verify you're on the new branch by type in:  `git branch`  

* Now to add a new file! Go ahead and do that with the following command:  `echo "Branched file" >> new_branch_file.txt`  

* Don't forget to add and commit your file:  `git add new_branch_file.txt`  and then `git commit -m "Adding new file"`  

    Note: You can always type in `git status` to see the current status of your work.    

* Now your remote repo on Bitbucket does not have this branch yet so you will need to create it upstream. Use the following command to do so and push the changes upstream:  `git push --set-upstream origin development`  

    This tells git to push your code up to your `origin` url and put it on the `development` branch.  

* Login to Bitbucket and you'll see something similar to the image below:  

  ![Git push development](images/8_branching_and_merging.png)  

When you click on the `development` branch your dashboard will refresh and you'll see the new file committed and tracked in Bitbucket!  

Go back to the `master` branch and you'll see that it does not have this file yet. This is a great way to work on code without breaking something that is being used in production on the master branch.  

### So let's merge them together now!  

----
**Navigation**

* [Previous ~ Pushing Your Code Upstream](5_pushing_your_code_upstream.md)
* [Next ~ Merging](7_merging_branches.md)
* [Table of Contents](README.md)