# README

Welcome to the _Getting Git!_ Webinar Repository!

- The `introduction_to_git` folder has all the markdown files that deal with an introduction to git, some terminology, etc.  

- The `lab_work` folder has all the exercises and the tutorial for you to follow along.  

### What is this repository for?

Feel free to fork this repo and use it as you learn how to navigate the Git waters. This repository contains the webinar materials, examples, and exercises that were part of the _Getting Git!_ webinar.

### Contribution guidelines

See a typo or suggestion for improvement? Then do a PR and submit your changes!

### Who do I talk to?

* You can reach me at - allen.vailliencourt@forty8fiftylabs.com
* Or contact us at www.forty8fiftylabs.com